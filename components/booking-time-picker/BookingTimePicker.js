import PropTypes from 'prop-types';

import styles from '../../styles/BookingTimePicker.module.css';

const BookingTimePicker = ({
  timeslots = [],
  isDateSelected = false,
  onTimeslotChanges,
}) => {
  const renderDatePickerOptions = (timeslots) => {
    return timeslots.map((time) => {
      return (
        <option
          key={time.id}
          value={time.id}
        >{`${time.startTime} - ${time.endTime}`}</option>
      );
    });
  };

  return (
    <div className={styles.bookingTimePicker}>
      <select
        onChange={(e) => onTimeslotChanges(e.target.value)}
        className={styles.bookingTimePickerSelect}
        disabled={!isDateSelected}
      >
        <option defaultValue={0}>Select timeslot</option>
        {renderDatePickerOptions(timeslots)}
      </select>
    </div>
  );
};

BookingTimePicker.propTypes = {
  timeslots: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      startTime: PropTypes.string,
      endTime: PropTypes.string,
    }),
  ),
  isDateSelected: PropTypes.bool,
  onTimeslotChanges: PropTypes.func,
};

export default BookingTimePicker;
