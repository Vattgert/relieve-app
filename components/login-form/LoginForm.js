import { Form, Field } from 'react-final-form';
import Link from 'next/link';
import Router from 'next/router';
import relieveApi from '../../services/relieveAPI';
import styles from '../../styles/LoginForm.module.css';

const LoginForm = () => (
  <div className={styles.wrapper}>
    <Form
      onSubmit={async (values) => {
        const { status } = await relieveApi.login(values);
        if (status === 'ok') {
          Router.push('/');
        }
      }}
      validate={() => {
        console.log('Validate login.');
      }}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} className={styles.form}>
          <h2 className={styles.formTitle}>Login</h2>
          <div className={styles.inputWrapper}>
            <Field
              name="email"
              component="input"
              placeholder="Email"
              className={styles.input}
            />
          </div>
          <div className={styles.inputWrapper}>
            <Field
              name="password"
              component="input"
              placeholder="Password..."
              className={styles.input}
            />
          </div>
          <button type="submit" className={styles.submit}>
            Login
          </button>
        </form>
      )}
    />
    <span className={styles.signup}>
      {`Don't have an account?`}
      <Link href="/signup">
        <a> Sign up!</a>
      </Link>
    </span>
  </div>
);

export { LoginForm };
