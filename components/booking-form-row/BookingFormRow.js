import PropTypes from 'prop-types';
import styles from '../../styles/BookingFormRow.module.css';

const BookingFormRow = ({ title, children }) => {
  return (
    <div className={styles.bookingFormRow}>
      <div className={styles.bookingFormRowTitle}>{title}</div>
      <div className={styles.bookingFormRowContent}>{children}</div>
    </div>
  );
};

BookingFormRow.propTypes = {
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default BookingFormRow;
