import PropTypes from 'prop-types';
import styles from '../../styles/BookingPaymentSummary.module.css';

const BookingPaymentSummary = ({
  totalPrice,
  currency,
  onPurchaseButtonClick,
}) => {
  const getTransactionFee = (itemsPrice) => {
    return (itemsPrice * 2) / 100;
  };

  const transactionFee = getTransactionFee(totalPrice);

  return (
    <div className={styles.paymentSummaryContainer}>
      <div className={styles.paymentSummary}>
        <div className={styles.paymentSummaryRow}>
          <span>Item Price:</span>
          <span>
            {totalPrice}
            {currency.sign}
          </span>
        </div>
        <div className={styles.paymentSummaryRow}>
          <span>Transaction fee:</span>
          <span>
            {transactionFee}
            {currency.sign}
          </span>
        </div>
        <hr className={`${styles.delimeter} ${styles.delimeterBottomMargin}`} />
        <div className={styles.paymentSummaryRow}>
          <span>Total</span>
          <span>
            {transactionFee + totalPrice}
            {currency.sign}
          </span>
        </div>
      </div>
      <button
        type="submit"
        className={styles.purchaseButton}
        onClick={onPurchaseButtonClick}
      >
        Purchase
      </button>
    </div>
  );
};

BookingPaymentSummary.propTypes = {
  totalPrice: PropTypes.number,
  currency: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    sign: PropTypes.string,
    code: PropTypes.string,
  }),
  onPurchaseButtonClick: PropTypes.func,
};

export default BookingPaymentSummary;
