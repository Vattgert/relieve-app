import PropTypes from 'prop-types';
import Link from 'next/link';

import styles from '../../styles/CategoriesHeader.module.css';

export const CategoriesHeader = ({ categories }) => {
  function renderCategoriesHeaderItem({ id, title, codename }) {
    return (
      <li className={styles.item} key={id}>
        <Link href={`/categories/${codename}`}>
          <a>{title}</a>
        </Link>
      </li>
    );
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.top}>
        <span>Top categories</span>
      </div>
      <ul className={styles.list}>
        {categories.map(renderCategoriesHeaderItem)}
      </ul>
    </div>
  );
};

CategoriesHeader.propTypes = {
  categories: PropTypes.array.isRequired,
};
