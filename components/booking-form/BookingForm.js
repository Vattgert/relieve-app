import { useState } from 'react';
import { BookingFormRow } from '../booking-form-row';
import { BookingTimePicker } from '../booking-time-picker';
import { BookingUserForm } from '../booking-user-form';
import { Variation } from '../variation';
import { CreditCardForm } from '../credit-card-form';
import { BookingPaymentSummary } from '../booking-payment-summary';

import Calendar from 'react-calendar';

import styles from '../../styles/BookingForm.module.css';
import PropTypes from 'prop-types';
import relieveApi from '../../services/relieveAPI';

import { dateUtils } from '../../utils';
const { isTheSameDate, dateToStringFormat } = dateUtils;

function availableDatesForCalendar(schedules) {
  const availableDates = schedules.map((entry) => {
    if (entry['date']) {
      return entry['date'];
    }
  });

  return function ({ date, view }) {
    if (view === 'month') {
      const isDateAvailable = availableDates.find((avDate) =>
        isTheSameDate(avDate, date),
      );

      return !isTheSameDate(isDateAvailable, date);
    }
  };
}

const showDateTimeslots = (schedules) => (selectDate, selectTimeslot) => (
  date,
) => {
  const dateSchedule = schedules.find((schedule) => {
    if (schedule['date']) {
      return isTheSameDate(date, new Date(schedule['date']));
    }
  });

  if (dateSchedule) {
    selectDate(date);
    const dateString = dateToStringFormat(date);
    const timeslots = schedules.map((schedule) => {
      if (schedule['date'] === dateString) {
        return schedule['timeslot'];
      }
    });
    selectTimeslot(timeslots);
  }
};

const showVariations = (schedules) => (
  selectedDate,
  setVariations,
  setCurrency,
  setItemsPrice,
  setTimeslot,
) => (timeslotId) => {
  const formattedDate = dateToStringFormat(selectedDate);
  const selectedSchedule = schedules.find((schedule) => {
    return (
      schedule['date'] === formattedDate &&
      schedule['timeslot']['id'] === timeslotId
    );
  });

  if (selectedSchedule) {
    const {
      variations,
      currency,
      timeslot: { id },
    } = selectedSchedule;

    setTimeslot(id);
    setVariations(variations);
    setCurrency(currency);
    setItemsPrice(0);
  } else {
    setVariations([]);
  }
};

function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}

const BookingForm = ({ schedules, userInfo = {} }) => {
  const [selectedDate, setDate] = useState('');
  const [selectedTimeslot, setTimeslot] = useState(0);
  const [selectedVariations, setSelectedVariations] = useState([]);
  const [creditCard, setCreditCard] = useState({});
  const [user, setUser] = useState(userInfo);

  const [timeSlots, setTimeslots] = useState([]);
  const [variations, setVariations] = useState([]);
  const [currency, setCurrency] = useState({});
  const [itemsPrice, setItemsPrice] = useState(0);

  const setBookingDateAndTimeslots = showDateTimeslots(schedules);
  const setVariationData = showVariations(schedules);

  const onVariationQuantityChange = (variationObject) => {
    const countTotalPrice = (variations) => {
      let totalPrice = 0;
      for (const variation of variations) {
        totalPrice += variation.quantity * variation.price;
      }
      return totalPrice;
    };

    const existingVariation = selectedVariations.find(
      (variation) => variationObject.id === variation.id,
    );

    const variationsCopy = [...selectedVariations];
    if (existingVariation) {
      if (variationObject.quantity === 0) {
        const indexOfExistingVariation = selectedVariations.indexOf(
          existingVariation,
        );
        variationsCopy.splice(indexOfExistingVariation, 1);
        setSelectedVariations(variationsCopy);
      } else {
        const indexOfExistingVariation = selectedVariations.indexOf(
          existingVariation,
        );
        variationsCopy[indexOfExistingVariation]['quantity'] =
          variationObject.quantity;
        setSelectedVariations(variationsCopy);
      }
    } else {
      variationsCopy.push(variationObject);
      setSelectedVariations(variationsCopy);
    }
    setItemsPrice(countTotalPrice(variationsCopy));
  };

  const onGetCreditCardData = (values) => {
    setCreditCard(values);
  };

  const onUserData = (input) => (event) => {
    const newUserState = user ? { ...user } : {};
    newUserState[input] = event.target.value;
    setUser(newUserState);
  };

  const onPurchaseButtonClick = async () => {
    const bookingInfoObject = {};
    bookingInfoObject['activityId'] = '1';
    bookingInfoObject['date'] = dateToStringFormat(selectedDate);
    bookingInfoObject['timeslotId'] = selectedTimeslot;
    bookingInfoObject['variations'] = selectedVariations;
    bookingInfoObject['creditCard'] = creditCard;
    bookingInfoObject['user'] = user;
    console.log(bookingInfoObject);
    console.log(JSON.stringify(bookingInfoObject));
    //relieveApi.bookActivity(bookingInfoObject);
  };

  function renderVariations(variations, currency) {
    return variations.map((variation) => (
      <Variation
        key={variation.id}
        variation={variation}
        currency={currency}
        onVariationQuantityChange={onVariationQuantityChange}
      />
    ));
  }

  function renderPurchaseSummary(isVariations, currency, totalPrice = 0) {
    if (isVariations) {
      return (
        <BookingPaymentSummary
          currency={currency}
          totalPrice={totalPrice}
          onPurchaseButtonClick={onPurchaseButtonClick}
        />
      );
    }
  }

  return (
    <div className={styles.bookingForm}>
      <BookingFormRow title={'Select date and time'}>
        <div className={styles.bookingFormSchedulePicker}>
          <Calendar
            className={styles.calendar}
            tileDisabled={availableDatesForCalendar(schedules)}
            locale={'en-US'}
            onClickDay={setBookingDateAndTimeslots(setDate, setTimeslots)}
          />
          <BookingTimePicker
            timeslots={timeSlots}
            isDateSelected={isValidDate(selectedDate)}
            onTimeslotChanges={setVariationData(
              selectedDate,
              setVariations,
              setCurrency,
              setItemsPrice,
              setTimeslot,
            )}
          />
        </div>
        <div className={styles.bookingVariations}>
          {renderVariations(variations, currency)}
        </div>
      </BookingFormRow>
      <hr className={styles.delimeter} />
      <BookingFormRow title={'Enter user data'}>
        <BookingUserForm userInfo={userInfo} onUserData={onUserData} />
      </BookingFormRow>
      <hr className={styles.delimeter} />
      <BookingFormRow title={'Enter credit card'}>
        <CreditCardForm onFormGetData={onGetCreditCardData} />
      </BookingFormRow>
      <hr className={styles.delimeter} />
      <BookingFormRow title={'Payment summary'}>
        {renderPurchaseSummary(variations.length > 0, currency, itemsPrice)}
      </BookingFormRow>
    </div>
  );
};

BookingForm.propTypes = {
  schedules: PropTypes.array.isRequired,
  userInfo: PropTypes.object,
};

export default BookingForm;
