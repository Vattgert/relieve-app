import PropTypes from 'prop-types';
import { useState } from 'react';

import styles from '../../styles/Variation.module.css';

const Variation = ({ variation, currency, onVariationQuantityChange }) => {
  const [guestsNumber, setGuestsNumber] = useState(0);
  const [isMaxed, setIsMaxed] = useState(false);

  const ADD_ACTION = 'add',
    REMOVE_ACTION = 'remove';

  const changeQuantinyNumber = (action) => () => {
    const quantity = parseInt(variation.quantity);

    let guestsNumberLocal = guestsNumber;
    if (action === ADD_ACTION) {
      if (++guestsNumberLocal <= quantity) {
        if (guestsNumberLocal === quantity) {
          setIsMaxed(true);
        }
        setGuestsNumber(guestsNumberLocal);
        onVariationQuantityChange({
          id: variation.id,
          quantity: guestsNumberLocal,
          title: variation.title,
          price: variation.price,
        });
      }
    } else if (action === REMOVE_ACTION) {
      if (!(--guestsNumberLocal < 0)) {
        if (isMaxed) {
          setIsMaxed(false);
        }
        setGuestsNumber(guestsNumberLocal);
        onVariationQuantityChange({
          id: variation.id,
          quantity: guestsNumberLocal,
          title: variation.title,
          price: variation.price,
        });
      }
    }
  };

  return (
    <div className={styles.variationRow}>
      <div className={styles.variation}>
        <span className={styles.variationName}>{variation.title}</span>
        <span className={styles.variationPrice}>
          {variation.price}
          {currency.sign}
        </span>
      </div>
      <div className={styles.guests}>
        <div className={styles.guestsNumberPickerContainer}>
          <div className={styles.guestsNumberPicker}>
            <button
              className={`${styles.guestsButton} ${styles.remove} ${
                guestsNumber === 0 ? styles.disabled : ''
              }`}
              onClick={changeQuantinyNumber(REMOVE_ACTION)}
            >
              -
            </button>
            <div className={styles.guestsNumber}>{guestsNumber}</div>
            <button
              className={`${styles.guestsButton} ${styles.add} ${
                isMaxed ? styles.disabled : ''
              }`}
              onClick={changeQuantinyNumber(ADD_ACTION)}
            >
              +
            </button>
          </div>
          <div className={`${styles.maxed} ${!isMaxed ? styles.disabled : ''}`}>
            Maxed!
          </div>
        </div>

        <div className={styles.totalPrice}>0$</div>
      </div>
    </div>
  );
};

Variation.propTypes = {
  variation: PropTypes.shape({
    id: PropTypes.string,
    quantity: PropTypes.number,
    title: PropTypes.string,
    price: PropTypes.string,
  }),
  currency: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    sign: PropTypes.string,
    code: PropTypes.string,
  }),
  onVariationQuantityChange: PropTypes.func,
};

export default Variation;
