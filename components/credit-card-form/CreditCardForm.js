import Payment from 'payment';
import { Form, Field, FormSpy } from 'react-final-form';
import PropTypes from 'prop-types';
import styles from '../../styles/CreditCardForm.module.css';

const ValidationError = ({ name }) => (
  <Field name={name} subscription={{ error: true }}>
    {({ meta: { error } }) =>
      error ? <span className={styles.validationError}>{error}</span> : null
    }
  </Field>
);

const CreditCardForm = ({ onFormGetData }) => {
  function clearNumber(value = '') {
    return value.replace(/\D+/g, '');
  }

  function formatCreditCardNumber(value) {
    if (!value) {
      return '';
    }

    const issuer = Payment.fns.cardType(value);
    const clearValue = clearNumber(value);
    let nextValue = '';

    switch (issuer) {
      case 'amex':
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(
          4,
          10,
        )} ${clearValue.slice(10, 15)}`;
        break;
      case 'dinersclub':
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(
          4,
          10,
        )} ${clearValue.slice(10, 14)}`;
        break;
      default:
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(
          4,
          8,
        )} ${clearValue.slice(8, 12)} ${clearValue.slice(12, 19)}`;
        break;
    }

    return nextValue.trim();
  }

  function formatExpirationDate(value) {
    const clearValue = clearNumber(value);

    if (clearValue.length >= 3) {
      return `${clearValue.slice(0, 2)}/${clearValue.slice(2, 4)}`;
    }

    return clearValue;
  }

  function formatCVC(value, prevValue, allValues = {}) {
    const clearValue = clearNumber(value);
    let maxLength = 4;

    if (allValues.number) {
      const issuer = Payment.fns.cardType(allValues.number);
      maxLength = issuer === 'amex' ? 4 : 3;
    }

    return clearValue.slice(0, maxLength);
  }

  const validateCreditCardNumber = (value) => {
    return Payment.fns.validateCardNumber(value);
  };

  const validateCvcCode = (value) => {
    return Payment.fns.validateCardCVC(value);
  };

  const validateExpiringDate = (value) => {
    return Payment.fns.validateCardExpiry(value);
  };

  return (
    <div className={styles.creditCardFormContainer}>
      <Form
        className={styles.creditCardForm}
        onSubmit={() => {}}
        validate={(values) => {
          const errors = {};
          if (!values.cardNumber) {
            errors.cardNumber = 'Required';
          } else if (!validateCreditCardNumber(values.cardNumber)) {
            errors.cardNumber = 'Credit card number has unappropriate format';
          }
          if (!values.cvcCode) {
            errors.cvcCode = 'Required';
          } else if (!validateCvcCode(values.cvcCode)) {
            errors.cvcCode = 'CVC code has unappropriate format';
          }
          if (!values.expiringDate) {
            errors.expiringDate = 'Required';
          } else if (!validateExpiringDate(values.expiringDate)) {
            errors.expiringDate = 'Expiring date has unappropriate format';
          }
          return errors;
        }}
        render={() => {
          return (
            <form
              onSubmit={() => {}}
              id="credit-card-form"
              className={styles.creditCardForm}
            >
              <div className={styles.inputContainer}>
                <Field
                  name="cardNumber"
                  component="input"
                  type="text"
                  pattern="[\d| ]{16,22}"
                  placeholder="Card Number"
                  format={formatCreditCardNumber}
                  defaultValue={''}
                />
                <ValidationError name="cardNumber" />
              </div>
              <div className={styles.inputContainer}>
                <Field
                  name="cardName"
                  component="input"
                  type="text"
                  placeholder="Name"
                />
                <ValidationError name="cardName" />
              </div>
              <div className={styles.row}>
                <div className={styles.inputContainer}>
                  <Field
                    name="expiringDate"
                    component="input"
                    type="text"
                    pattern="\d\d/\d\d"
                    placeholder="Expiring date"
                    format={formatExpirationDate}
                  />
                  <ValidationError name="expiringDate" />
                </div>
                <div className={styles.inputContainer}>
                  <Field
                    name="cvcCode"
                    component="input"
                    type="text"
                    pattern="\d{3,4}"
                    placeholder="CVC"
                    format={formatCVC}
                  />
                  <ValidationError name="cvcCode" />
                </div>
              </div>
              <FormSpy
                subscription={{ values: true, valid: true }}
                onChange={async (state) => {
                  const { values } = state;
                  setTimeout(() => {
                    onFormGetData(values);
                  }, 0);
                }}
              />
            </form>
          );
        }}
      />
    </div>
  );
};

CreditCardForm.propTypes = {
  onFormGetData: PropTypes.func,
  subscription: PropTypes.func,
};

ValidationError.propTypes = {
  name: PropTypes.string,
};

export default CreditCardForm;
