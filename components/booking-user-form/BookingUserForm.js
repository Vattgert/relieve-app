import PropTypes from 'prop-types';
import styles from '../../styles/BookingUserForm.module.css';

const BookingUserForm = ({ userInfo = {}, onUserData }) => {
  const { firstName, lastName, email, phoneNumber } = userInfo;

  return (
    <div className={styles.contactInfo}>
      <div className={styles.contactInfoRow}>
        <div className={styles.textInputContainer}>
          <input
            type="text"
            placeholder="First Name"
            className={styles.textInput}
            defaultValue={firstName || ''}
            onChange={onUserData('firstName')}
          />
        </div>
        <div className={styles.textInputContainer}>
          <input
            type="text"
            placeholder="Email"
            className={styles.textInput}
            defaultValue={email || ''}
            onChange={onUserData('email')}
          />
        </div>
      </div>
      <div className={styles.contactInfoRow}>
        <div className={styles.textInputContainer}>
          <input
            type="text"
            placeholder="Last Name"
            className={styles.textInput}
            defaultValue={lastName || ''}
            onChange={onUserData('lastName')}
          />
        </div>
        <div className={styles.textInputContainer}>
          <input
            type="text"
            placeholder="Phone number"
            className={styles.textInput}
            defaultValue={phoneNumber || ''}
            onChange={onUserData('phoneNumber')}
          />
        </div>
      </div>
    </div>
  );
};

BookingUserForm.propTypes = {
  userInfo: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    email: PropTypes.string,
    phoneNumber: PropTypes.string,
  }),
  onUserData: PropTypes.func,
};

export default BookingUserForm;
