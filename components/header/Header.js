import Link from 'next/link';
import { SearchInput } from '../search-input';
import { MainLogo } from '../main-logo';
import PropTypes from 'prop-types';
import styles from '../../styles/Header.module.css';
import relieveAPI from '../../services/relieveAPI';
import Router from 'next/router';

const Header = ({ isLoggedIn, isHost, user }) => {
  async function logout() {
    const { status } = await relieveAPI.logout();
    if (status === 'ok') {
      Router.reload();
    }
  }

  function renderNavigationLinkItem(href, text, requestHandler) {
    if (!requestHandler) {
      return (
        <li className={styles.navigationItem}>
          <Link href={href}>
            <a className={styles.navigationLink}>{text}</a>
          </Link>
        </li>
      );
    } else {
      return (
        <li className={styles.navigationItem}>
          <div onClick={requestHandler} className={styles.navigationLink}>
            {text}
          </div>
        </li>
      );
    }
  }

  return (
    <div className={styles.header}>
      <MainLogo />
      <SearchInput />
      <ul className={styles.navigation}>
        {isHost
          ? renderNavigationLinkItem('/business', 'My Business')
          : renderNavigationLinkItem('/business', 'Become a host')}
        {renderNavigationLinkItem('/giftcard', 'Gift card')}
        {isLoggedIn
          ? renderNavigationLinkItem(
              '/me',
              `${user.firstName} ${user.lastName}`,
            )
          : ''}
        {isLoggedIn
          ? renderNavigationLinkItem('/api/logout', 'Logout', logout)
          : renderNavigationLinkItem('/login', 'Login')}
      </ul>
    </div>
  );
};

Header.propTypes = {
  isLoggedIn: PropTypes.bool,
  isHost: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
};

export { Header };
