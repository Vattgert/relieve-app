import { get, post } from './Api';

class RelieveApi {
  constructor() {
    this.API_BASE_URL = 'http://api.relieve.com:3000/v1';
  }

  getActivities(params) {
    return get(this.API_BASE_URL + '/activities', params);
  }

  getActivity(activityId) {
    return get(this.API_BASE_URL + `/activities/${activityId}`);
  }

  getTopCategories() {
    return get(this.API_BASE_URL + '/categories/top');
  }

  getUserProfile(userId) {
    return get(this.API_BASE_URL + `/profiles/${userId}`);
  }

  getAcitivityVotes(activityId) {
    return get(this.API_BASE_URL + `/activities/${activityId}/votes`);
  }

  getActivitySchedules(activityId) {
    return get(`${this.API_BASE_URL}/activities/${activityId}/schedules`);
  }

  bookActivity(activityPayload) {
    const { activityId } = activityPayload;
    return post(`${this.API_BASE_URL}/booking/${activityId}`, activityPayload);
  }

  login(credentials) {
    return post(`/api/login`, credentials);
  }

  logout() {
    return post(`/api/logout`);
  }

  getSessionUser() {
    return get('/api/me');
  }
}

export default new RelieveApi();
