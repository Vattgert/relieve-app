export const API_BASE_URL = 'http://api.relieve.com:3000/v1';

export async function send({ method, path, body, cookies }) {
  const url = path;

  const fetchOptions = {
    mode: 'cors',
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'htpp://api.relieve.com:3000',
    },
    credentials: 'include',
  };

  if (cookies) {
    Object.assign(fetchOptions.headers, cookies);
  }

  if (body) {
    fetchOptions['body'] = JSON.stringify(body);
  }

  return fetch(url, fetchOptions);
}

const defaultResponse = (response) => {
  if (response.ok) {
    return response.json();
  }
};

function createParamsString(params) {
  let paramsString = '';
  if (params) {
    Object.keys(params).map((key, index, array) => {
      if (key && params[key]) {
        paramsString += `${key}=${params[key]}`;
      } else if (key && !params[key]) {
        paramsString += `${key}`;
      }

      if (index !== array.length - 1) {
        paramsString += '&';
      }
    });
  }
  return paramsString ? `?${paramsString}` : '';
}

export async function get(path, params) {
  const paramsString = createParamsString(params);

  const response = await send({
    method: 'get',
    path: `${path}${paramsString}`,
  });

  return defaultResponse(response);
}

export async function post(path, body) {
  const response = await send({
    method: 'post',
    path,
    body,
  });

  return defaultResponse(response);
}
