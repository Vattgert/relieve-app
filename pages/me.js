import useUser from '../lib/hooks/useUser';

const Me = () => {
  const { user, isLoggedIn } = useUser({ redirectTo: '/login' });
  return isLoggedIn && user ? <div>This is session for user </div> : '';
};

export default Me;
