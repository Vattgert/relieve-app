import { LoginForm } from '../components/login-form';
import { Header } from '../components/header';
import styles from '../styles/Login.module.css';
import useUser from '../lib/hooks/useUser';

export default function Login() {
  const { user, isLoggedIn } = useUser({ redirectIfSession: '/' });

  return (
    <div className="page">
      <Header isLoggedIn={isLoggedIn} isHost={false} user={user} />
      <div className={styles.container}>
        <LoginForm />
      </div>
    </div>
  );
}
