import { useEffect, useState } from 'react';

import { Header } from '../../components/header';
import { Footer } from '../../components/footer';
import { RoundedButton } from '../../components/rounded-button';
import { UpvoteItem } from '../../components/upvote-item';
import { LastLikes } from '../../components/last-likes';

import serviceApi from '../../services/relieveAPI';
import { useQuery } from '../../utils/useQuery';

import styles from '../../styles/Activity.module.css';
import useUser from '../../lib/hooks/useUser';

const Activity = () => {
  const query = useQuery();
  const [activity, setActivity] = useState(null);
  const [votes, setVotes] = useState([]);
  const { user, isLoggedIn } = useUser();

  useEffect(() => {
    if (query) {
      const { activityId } = query;
      serviceApi.getActivity(activityId).then(setActivity);
      serviceApi.getAcitivityVotes(activityId).then(setVotes);
    }
  }, [query]);

  function renderTags(tags) {
    return (
      <ul>
        {tags.map((tagItem) => {
          const { id, tag } = tagItem;
          return (
            <li key={id}>
              <RoundedButton
                text={tag}
                link={`tags/${encodeURIComponent(tag.toLowerCase())}`}
              />
            </li>
          );
        })}
      </ul>
    );
  }

  function renderVotes(votes) {
    return (
      <ul>
        {votes.map((vote) => {
          return (
            <li key={vote.id}>
              <UpvoteItem vote={vote} />
            </li>
          );
        })}
      </ul>
    );
  }

  if (!activity) return <div>Temporarily no data</div>;

  const {
    title,
    date,
    banner,
    country,
    city,
    summary,
    host: { firstName, lastName },
    tags,
    lastLikes,
    totalLikes,
  } = activity;

  const location = `${country}, ${city}`;
  const hostFullName = `${firstName} ${lastName}`;

  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  };
  const activityDate = new Intl.DateTimeFormat('en-US', dateOptions).format(
    new Date(date),
  );

  return (
    <div className="page">
      <Header isLoggedIn={isLoggedIn} isHost={false} user={user} />
      <div className="container">
        <div className={styles.container}>
          <div className={styles.header}>
            <h2>
              Outside * Tours | {hostFullName} | {location}
            </h2>
            {<LastLikes lastLikes={lastLikes} totalLikes={totalLikes} />}
          </div>
          <div className={styles.content}>
            <div className={styles.image}>
              <img src={banner} alt="rome" />
            </div>
            <div className={styles.details}>
              <div className={styles.mainDetails}>
                <h3 className={styles.title}>{title}</h3>
                <span className={styles.date}>{activityDate}</span>
              </div>
              <div className={styles.description}>
                <span>{summary}</span>
              </div>
              <div className={styles.tags}>{renderTags(tags)}</div>
            </div>
            <div className={`${styles.lastUpvotes}`}>
              <div className={styles.header}>
                <span className={styles.title}>Last User Votes</span>
              </div>
              {renderVotes(votes)}
            </div>
          </div>
          <div className={styles.similarActivities}>
            <div className={styles.header}></div>
            <ul className={styles.activitiesList}>
              {/*<li><ActivityCard activity={act}/></li>
                                <li><ActivityCard activity={act}/></li>
                                <li><ActivityCard activity={act}/></li>
                                <li><ActivityCard activity={act}/></li>*/}
            </ul>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Activity;
