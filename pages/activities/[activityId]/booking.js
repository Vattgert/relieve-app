import { useEffect, useState } from 'react';
import { useQuery } from '../../../utils/useQuery';
import useUser from '../../../lib/hooks/useUser';

import { Header } from '../../../components/header';
//import { Footer } from '../../../components/footer';
import { BookingForm } from '../../../components/booking-form';

import relieveApi from '../../../services/relieveAPI';

const Booking = () => {
  const query = useQuery();
  const { user, isLoggedIn } = useUser();
  const [schedules, setSchedules] = useState([]);

  useEffect(() => {
    if (query) {
      const { activityId } = query;
      relieveApi.getActivitySchedules(activityId).then(setSchedules);
    }
  }, [query]);

  return (
    <div className="page d-flex-align-center">
      <Header isLoggedIn={isLoggedIn} isHost={false} user={user} />
      <BookingForm schedules={schedules} userInfo={user} />
    </div>
  );
};

export default Booking;
