import { post, API_BASE_URL } from '../../services/Api';
import { redisSessionStorage } from '../../lib/session';
import Cookies from 'cookies';

export default async function loginHandler(req, res) {
  if (req.method === 'POST') {
    const { body } = req;
    const cookie = new Cookies(req, res);

    if (body) {
      const currentSessionId = cookie.get('sessionId');
      if (currentSessionId) {
        const sessionExists = await redisSessionStorage.sessionExists(
          currentSessionId,
        );

        if (sessionExists) {
          res
            .status(200)
            .send({ status: false, message: 'You are already logged in' });
        }
      } else {
        const path = API_BASE_URL + '/auth';
        const sessionInfo = await post(path, body);
        if (sessionInfo) {
          const newSessionId = redisSessionStorage.createSessionId();
          const currentDate = new Date();
          const expiringDate = new Date();
          expiringDate.setTime(currentDate.getTime() + 60 * 1000 * 30);
          const expires = expiringDate.toUTCString();
          const sessionTimeToLive =
            expiringDate.getTime() - currentDate.getTime();

          const cookieString = `sessionId=${newSessionId}; Path=/; HttpOnly; Expires=${expires}`;
          redisSessionStorage.saveSessionData(
            newSessionId,
            sessionInfo,
            sessionTimeToLive,
          );
          res.setHeader('set-cookie', cookieString);

          res.status(200).send({ status: 'ok' });
        }
      }
    }
  }
}
