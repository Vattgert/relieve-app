import Cookies from 'cookies';
import { redisSessionStorage } from '../../lib/session';

export default async function logoutHandler(req, res) {
  if (req.method === 'POST') {
    const cookies = new Cookies(req, res);
    const sessionId = cookies.get('sessionId');
    if (sessionId) {
      const isSessionExists = await redisSessionStorage.sessionExists(
        sessionId,
      );
      if (isSessionExists) {
        await redisSessionStorage.destroySession(sessionId);
        const date = new Date();
        cookies.set('sessionId', '', { expires: date.toUTCString() });
        res.status(200).send({ status: 'ok' });
      } else {
        const date = new Date();
        cookies.set('sessionId', '', { expires: date.toUTCString() });
        res.status(200).send({
          status: 'false',
          message: 'You are not logged in to logout',
        });
      }
    } else {
      res
        .status(200)
        .send({ status: 'false', message: 'You are not logged in to logout' });
    }
  }
}
