import { redisSessionStorage } from '../../lib/session';
import Cookies from 'cookies';

export default async function getSessionUser(req, res) {
  if (req.method === 'GET') {
    const cookies = new Cookies(req, res);
    const sessionId = cookies.get('sessionId');
    if (sessionId) {
      const sessionData = await redisSessionStorage.getSessionData(sessionId);
      const user = JSON.parse(sessionData);
      if (user && user.id) {
        const result = {
          isLoggedIn: true,
          user,
        };

        res.status(200).send(result);
      } else {
        res.status(200).send({
          status: 'false',
          message: 'Malformed session data. Please, log in again',
        });
      }
    } else {
      res
        .status(200)
        .send({ status: 'false', message: 'You are not logged in' });
    }
  }
}
