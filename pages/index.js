import { Header } from '../components/header';
import { CategoriesHeader } from '../components/categories-header';
import { ActivityCard } from '../components/activity-card';

import PropTypes from 'prop-types';

import serviceApi from '../services/relieveAPI';

import styles from '../styles/Home.module.css';
import useUser from '../lib/hooks/useUser';

export default function Home({ activities, categories }) {
  const { user, isLoggedIn } = useUser();

  function renderTours(activities) {
    return activities.map((activity) => (
      <li key={activity.id}>
        <ActivityCard activity={activity} />
      </li>
    ));
  }

  return (
    <div className="page">
      <Header isLoggedIn={isLoggedIn} isHost={false} user={user} />
      <CategoriesHeader categories={categories} />
      {
        <div className={`${styles.container} ${styles.innerContainer}`}>
          <div>
            <ul className={styles.activitiesList}>{renderTours(activities)}</ul>
          </div>
        </div>
      }
    </div>
  );
}

export async function getStaticProps() {
  const activities = await serviceApi.getActivities();
  const categories = await serviceApi.getTopCategories();

  return {
    props: {
      activities,
      categories,
    },
  };
}

Home.propTypes = {
  activities: PropTypes.array.isRequired,
  categories: PropTypes.array.isRequired,
};
