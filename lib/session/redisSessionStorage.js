import redis from 'redis';
import { promisify } from 'util';

export class RedisSessionStorage {
  constructor() {
    const redisConnection = {
      host: 'localhost',
      port: 6379,
    };
    this.redisClient = new redis.createClient(redisConnection);
    this.sessionConfig = {
      sessionIdLength: 40,
      timeToLive: 120,
    };

    const redisClientReference = this.redisClient;
    this.redisPromisified = {
      set: promisify(redisClientReference.set).bind(redisClientReference),
      setex: promisify(redisClientReference.setex).bind(redisClientReference),
      get: promisify(redisClientReference.get).bind(redisClientReference),
      exists: promisify(redisClientReference.exists).bind(redisClientReference),
      expire: promisify(redisClientReference.expire).bind(redisClientReference),
      destroy: promisify(redisClientReference.del).bind(redisClientReference),
      destroyAll: promisify(redisClientReference.flushall).bind(
        redisClientReference,
      ),
    };
  }

  createSessionId() {
    const result = [];
    const length = this.sessionConfig['sessionIdLength'];
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength)),
      );
    }
    return result.join('');
  }

  async saveSessionData(
    sessionId,
    sessionData,
    ttl = this.sessionConfig['timeToLive'],
  ) {
    const data = JSON.stringify(sessionData);
    const { setex } = this.redisPromisified;

    setex(sessionId, ttl, data);
  }

  refreshSession() {
    throw new Error('Method not implemented.');
  }

  async sessionExists(sessionId) {
    const { exists } = this.redisPromisified;
    return exists(sessionId);
  }

  async destroySession(sessionId) {
    const { destroy } = this.redisPromisified;
    destroy(sessionId);
  }

  async destroyAllSessions() {
    const { destroyAll } = this.redisPromisified;
    destroyAll();
  }

  async getSessionData(sessionId) {
    const { get } = this.redisPromisified;
    return get(sessionId);
  }
}
