import Router from 'next/router';
import relieveApi from '../../services/relieveAPI';
import { useEffect, useState } from 'react';

export default function useUser({
  redirectTo = false,
  redirectIfSession = false,
} = {}) {
  const [user, setUser] = useState(false);

  useEffect(() => {
    async function getUser() {
      const sessionUser = await relieveApi.getSessionUser();
      if (!sessionUser && !redirectTo) return;
      if (!sessionUser && redirectTo) {
        Router.push(redirectTo);
        return;
      }
      if (sessionUser && sessionUser.isLoggedIn && redirectIfSession) {
        Router.push(redirectIfSession);
      }
      setUser(sessionUser);
    }
    getUser();
  }, []);

  return user;
}
