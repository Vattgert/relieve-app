import { differenceInCalendarDays, format } from 'date-fns';

function isTheSameDate(d1, d2) {
  if (typeof d1 === 'string') {
    d1 = new Date(d1);
  }
  if (typeof d2 === 'string') {
    d2 = new Date(d2);
  }

  return differenceInCalendarDays(d1, d2) === 0;
}

function dateToStringFormat(date) {
  return format(date, 'y-MM-dd');
}

export { isTheSameDate, dateToStringFormat };
